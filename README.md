Collezione di script per Nautilus.
Per usarli
1. Assicurarsi di avere installato le dipendenze
```
sudo apt install python3-nautilus python3-gi
```
2. Trasferire i file in ~/.local/share/nautilus-python/extensions
3. Renderli eseguibili
```
chmod +x nomefile.py
```
4. Riavviare nautilus
```
nautilus -q
```
# GTK4 Requirements
```
pip install xerox
sudo apt install xclip
```