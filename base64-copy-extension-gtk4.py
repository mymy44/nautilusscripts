from gi.repository import Nautilus, GObject
import os
import base64
import logging
import xerox


class BaseCopyExtension(GObject.GObject, Nautilus.MenuProvider):

    def __init__(self):
        # uncomment to log stuff to a file
        # logging.basicConfig(filename="/home/ubuntu/pylog",
        #             filemode='a',
        #             format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
        #             datefmt='%H:%M:%S',
        #             level=logging.DEBUG)
        pass



    def get_file_items(self, *args):
        logging.info("in get file items")
        files = args[-1]
        item = Nautilus.MenuItem(
            name='b64Open',
            label='b64',
            tip='Opens '
        )
        logging.info("in get file items before connect")
        item.connect('activate', self._doStuff, files[0])

        return [item]

    def get_background_items(self, *args):
        logging.info("in get background")
        file_ = args[-1]
        item = Nautilus.MenuItem(
            name='b64OpenBackground',
            label='b64',
            tip='Opens '
        )
        logging.info("in get background before connect")
        item.connect('activate', self._doStuff, file_)

        return [item]


    def _doStuff(self, menu, filepath):  # LA TUA LOGICA VA QUI!
        logging.info("in do stuff")
        #clipboard=Gdk.Display.get_clipboard(Gdk.Display.get_default())
        logging.info("after clipboard")
        filepath = filepath.get_location().get_path()
        logging.info("got filepath " + filepath)
        if not os.path.isdir(filepath) and os.path.exists(filepath):
            with open(filepath, 'rb') as f:
                b64 = base64.b64encode(f.read()).decode('ascii')
                logging.info("got b64: "+b64)
                xerox.copy(b64)
                
