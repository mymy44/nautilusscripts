import base64
import os
from gi.repository import Nautilus, GObject, Gtk, Gdk
from gi import require_version

require_version('Gtk', '3.0')
require_version('Nautilus', '3.0')


class Base64CopyExtension(Nautilus.MenuProvider, GObject.GObject):

    def __init__(self):
        self.clipboard = Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD)

    def get_file_items(self, window, files):
        return self._create_menu_items(files, "File")

    def get_background_items(self, window, file):
        return self._create_menu_items([file], "Background")

    def _create_menu_items(self, files, group):
        filepath = files[0]
        item_path = Nautilus.MenuItem(
            name="b64Copy" + group,
            label="base64",
        )

        item_path.connect("activate", self._doStuff, filepath)
        return [item_path]

    def _doStuff(self, menu, filepath):  # LA TUA LOGICA VA QUI!
        filepath = filepath.get_location().get_path()
        if not os.path.isdir(filepath) and os.path.exists(filepath):
            with open(filepath, 'rb') as f:
                b64 = base64.b64encode(f.read()).decode('ascii')
                self.clipboard.set_text(b64, -1)
